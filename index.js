const fs = require("fs");
const path = require("path");
const { program } = require("commander");
program.version("0.1");

program.option(
  "-p, --preset <type>",
  "Select a predefined size between `sm`, `md`, `lg` and `xl`"
);
program.option("-j, --jobs <number>", "Specify number of jobs", 20);
program.option("-s, --stages <number>", "Specify number of stages", 5);
program.option("-n, --needs <number>", "Specify number of needs", 6);
program.option(
  "-i, --include <number>",
  "Specify number of files to includes",
  0
);

program.parse(process.argv);

const options = program.opts();
const {
  include: includeOptions,
  jobs: jobsOption,
  needs: needsOption,
  preset: presetOption,
  stages: stagesOption,
} = options;

const dummyStages = [
  "prepare",
  "optimize",
  "build",
  "pre-test",
  "test",
  "package",
  "documentation",
  "deploy",
  "smoke-test",
  "post-deploy",
];
let includeFileNames = [];
const extendJobName = ".extend_this_job";
const isArray = (val) => Boolean(val?.length >= 0 && typeof val === "object");
const existingJobs = {};

for (let i = 0; i < includeOptions; i += 1) {
  includeFileNames.push(`sub_file_${i}.yml`);
}

const sizeConversion = { sm: 5, md: 20, lg: 60, xl: 200 };
const stageConversion = {
  sm: 3,
  md: 5,
  lg: dummyStages.length - 3,
  xl: dummyStages.length,
};

function generateStages(dummyStages) {
  let stages = [];
  const stageSize = stageConversion[presetOption] || stagesOption;

  for (let i = 0; i < stageSize; i += 1) {
    let currentStageName = dummyStages[i];

    if (i >= dummyStages.length) {
      currentStageName = `${dummyStages[i % dummyStages.length]}_${i}`;
    }
    stages.push(currentStageName);
  }

  return stages;
}

// Create a hash of all the jobs.
function generateJobs(stages) {
  // Never go above the total amount of jobs required and
  // also make sure each stage has one job.
  const totalJobs = presetOption ? sizeConversion[presetOption] : jobsOption;
  const averageJobCount = Math.round(totalJobs / stages.length);
  let jobsCountdown = totalJobs;

  return stages.map((stage, index) => {
    const lastIteration = stages.length - 1 === index;
    const maxNumberOfJobs = jobsCountdown - (stages.length - index);

    const randomDiffentiator = Math.round(
      (Math.random() * totalJobs) / averageJobCount
    );
    const isPositive = Math.round(Math.random()) === 1;
    const nbOfJobs = isPositive
      ? averageJobCount + randomDiffentiator
      : averageJobCount - randomDiffentiator;

    /**
     * If it's the last iteration, we fill in the remaining number of jobs
     * If there are more jobs than the maximum we allow per stage, use the maximum
     */
    let ajustedNumberOfJobs = nbOfJobs;
    if (lastIteration) {
      ajustedNumberOfJobs = jobsCountdown;
    } else if (nbOfJobs >= maxNumberOfJobs) {
      ajustedNumberOfJobs = maxNumberOfJobs;
    } else if (nbOfJobs < 1) {
      ajustedNumberOfJobs = 1;
    }

    const stageJobs = [];
    for (let i = 1; i <= ajustedNumberOfJobs; i++) {
      stageJobs.push(`${stage}_${i}`);
    }
    // Count how many jobs are left
    jobsCountdown -= ajustedNumberOfJobs;

    return stageJobs;
  });
}

// With all the jobs created, now seed the needs
// which depends on the complexity of what the user wants.
function generateConfigData(jobsPerStage, stages) {
  let counter = null;
  let remainingNeedsCount = needsOption;

  return jobsPerStage.map((stageJobs, stageIndex) => {
    return stageJobs.map((jobName, jobIndex) => {
      counter = counter === null ? 0 : (counter += 1);
      const jobObject = generateJobData(counter, stages[stageIndex], jobName);

      const isLastJob =
        stageIndex === jobsPerStage.length - 1 &&
        jobIndex === stageJobs.length - 1;
      // The more along we are in stages, the more we want a high chance to
      // add needs.
      const stageFactor = stageIndex / stagesOption;
      const needsFactor = Math.random() + stageFactor;
      // 1.2 is a bit of a magic value and represent a number that is hard
      // enough to beat so that first stages have less chances to beat it,
      // but low enough for needs to be distributed
      const shouldHaveNeed = isLastJob || needsFactor > 1.2;

      existingJobs[jobObject.name] = { ...jobObject };

      if (shouldHaveNeed && remainingNeedsCount > 0 && stageIndex > 0) {
        const nbOfNeeds = generateRandomNeedsNumber(
          needsOption,
          remainingNeedsCount,
          isLastJob
        );
        const jobObjectWithNeeds = generateNeeds(
          jobObject,
          stageIndex,
          nbOfNeeds
        );

        remainingNeedsCount -= nbOfNeeds;
        return jobObjectWithNeeds;
      }

      return jobObject;
    });
  });

  function generateJobData(counter, stageName, jobName) {
    const jobList = [
      {
        name: jobName,
        extends: extendJobName,
      },
      {
        name: jobName,
        stage: stageName,
        script: "echo I run only on non-master branches",
        rules: [`if: '$CI_COMMIT_REF_NAME != "master"'`],
      },
      {
        name: jobName,
        stage: stageName,
        script: "echo I run only on master",
        rules: [`if: '$CI_COMMIT_REF_NAME == "master"'`],
      },
      {
        name: jobName,
        stage: stageName,
        before_script: [
          `"echo this runs after artifacts are collected, but before job executes"`,
        ],
        script: "echo I have an after and before script",
        after_script: [`"echo after the job executes, this will run"`],
      },
      {
        name: jobName,
        stage: stageName,
        script: "echo I am a manual job",
        rules: ["when: manual"],
      },
      {
        name: jobName,
        stage: stageName,
        script: [
          "echo I am allowed to failed, because I am a failure",
          "exit 1",
        ],
        allow_failure: true,
      },
      {
        name: jobName,
        stage: stageName,
        script: "echo I have nothing special",
      },
    ];
    const randomIndex = Math.round(Math.random() * (jobList.length - 1));
    const adjustedIndex = randomIndex === 0 ? 1 : randomIndex;
    const result = jobList[counter] ?? jobList[adjustedIndex];

    return result;
  }

  function generateRandomNeedsNumber(
    needsOption,
    remainingNeedsCount,
    isLastJob
  ) {
    if (isLastJob) {
      return remainingNeedsCount;
    }

    const randomNeedsNumber = Math.floor(Math.random() * 6);

    let adjustedNeedsCount = Math.min(
      randomNeedsNumber,
      remainingNeedsCount,
      needsOption
    );

    return adjustedNeedsCount;
  }

  function generateNeeds(jobObject, currentStageIndex, nbOfNeeds) {
    const job = { ...jobObject };

    // If we have 0 needs, add an empty array for instant execution
    if (nbOfNeeds === 0) {
      job.needs = [];

      return job;
    }
    // If we want to add needs and we aren't in the first stage,
    // we add some.
    // The needs always need to be between 0 and -1 of current index
    // because jobs can only need another job from a previous stage.
    job.needs = [];
    alreadyPickedNeedsNames = [];

    for (let i = 0; i < nbOfNeeds; i++) {
      const randomStageIndex = Math.round(
        Math.random() * (currentStageIndex - 1)
      );
      const randomStageJobsArray = jobsPerStage[randomStageIndex];
      let randomJob =
      randomStageJobsArray[
        Math.round(Math.random() * (randomStageJobsArray.length - 1))
        ];
      
      if (!alreadyPickedNeedsNames.includes(randomJob)) {
        alreadyPickedNeedsNames.push(randomJob)
        // If the job it depends on has `rules`, we want to set
        // optional: true
  
        const hasRules = Object.keys(existingJobs[randomJob]).includes("rules");
        job.needs.push({ name: randomJob, optional: hasRules });
      }
    }

    return job;
  }
}

function writeYmlCIConfigFile(configData, stages) {
  const dir = "./output";
  if (!fs.existsSync(dir)) {
    console.log("dir does not exists")
    fs.mkdirSync(dir);
  }
  console.log("EXISTS")
  const file = fs.createWriteStream(`${dir}/.gitlab-ci.yml`);
  file.on("error", function (err) {
    console.log(err);
  });

  const extendJob = `${extendJobName}:\n  script: "echo I am extended"\n  stage: ${stages[0]}\n\n`;

  file.write(`stages:\n  `);

  stages.forEach((stage) => {
    file.write(`- ${stage}\n  `);
  });

  file.write("\n");

  file.write(extendJob);

  configData.forEach((stageJobsObject) => {
    stageJobsObject.forEach((job) => {
      file.write(`${job.name}:\n  `);

      const jobKeywords = Object.keys(job);
      jobKeywords.forEach((keyword, index) => {
        if (keyword !== "name") {
          // if it's an array, we want to write it so that we add the dash
          // for each item. Needs however, we want to just render the list as is.
          const keywordValue = job[keyword];
          if (keyword === "needs") {
            // we start by writing all non-optional
            // needs as an array, and then the rest we
            // need to manually write each one on a separate
            // line to include optional
            const optionalNeeds = keywordValue.filter((need) => need.optional);
            const mandatoryNeeds = keywordValue.filter(
              (need) => !need.optional
            );
            file.write(`needs:\n  `);

            if (mandatoryNeeds.length) {
              file.write(
                `- ${JSON.stringify(
                  mandatoryNeeds.map((need) => need.name)
                )}\n  `
              );
            }
            optionalNeeds.forEach(({ name }, optionalIndex) => {
              const whitespace = optionalIndex === 0 ? "" : "  ";
              file.write(`${whitespace}- job: ${name}\n    `);
              file.write(`optional: true\n`);
            });
          } else if (isArray(keywordValue)) {
            file.write(`${keyword}:\n`);
            keywordValue.forEach((line, lineIndex) => {
              let lineText = `    - ${line}\n`;
              // We add 2 line indentation for last line
              if (lineIndex === keywordValue.length - 1) {
                lineText += "  ";
              }
              file.write(lineText);
            });
          } else {
            const lineText = generateSingleLineText(job, keyword, index);
            file.write(lineText);
          }
        }
      });

      file.write("\n");
    });
  });

  // Include the number of files specified
  if (includeOptions > 0) {
    file.write("include:\n");

    includeFileNames.forEach((filename, index) => {
      file.write(`  - local: includes/${filename}\n`);
    });
  }

  file.end();
}

function generateSingleLineText(job, currentKeyword, index) {
  const jobKeywordsLength = Object.keys(job).length;
  let lineText = `${currentKeyword}: ${JSON.stringify(job[currentKeyword])}\n`;

  // We add 2 line indentation for each line except the
  // last one to break out of the nested elements
  if (index !== jobKeywordsLength - 1) {
    lineText += "  ";
  }

  return lineText;
}

function writeIncludes(includeFileNames) {
  const dir = "./output/includes";
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  includeFileNames.forEach((filename, index) => {
    const file = fs.createWriteStream(path.join(dir, filename));

    file.on("error", function (err) {
      console.log(err);
    });

    file.write(
      `include_${index}:\n  script: echo "I am included job #${index}"`
    );

    file.end();
  });
}

/* EXECUTION OF SCRIPT */

// 1. Make new stages
const filteredStages = generateStages(dummyStages);
// 2. Make new jobs
const jobsPerStage = generateJobs(filteredStages);
// 3. Generate random needs data and return it as config data
const configData = generateConfigData(jobsPerStage, filteredStages);
// 4. Write to file
writeYmlCIConfigFile(configData, filteredStages)
// 5. Create included files if we need to
if (includeOptions > 0) {
  writeIncludes(includeFileNames);
}
