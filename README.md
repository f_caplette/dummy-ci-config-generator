# Dummy CI config generator

## Requirements

This script require NodeJs v14+

## Description

This utility generates a dummy gitlab CI config file. This is useful to test different
kind of configuration of gitlab-ci syntax itself and not specific languages or build
processes. For example, if you need to test a configuration of 1000 jobs in 6 stages with 200 needs
and 3 child pipelines, you can generate it with this script, so read on!

## Getting Started

- Clone the repository in a location of your choosing
- Make sure to have node installed on your machine. You can run `npm version` which
should give you the version if it's installed and make sure it is at least version 14.
- Run `npm install` in the directory where you cloned the project

## Executing the script

To run and receive a medium-sized, completly random file 
which includes a bit of everything gitlab CI config has to offer, you can run:

```js
node index.js
```

Generated config file will live in an `output/` directory created at the root 
of the repository. The file will be named `.gitlab-ci.yml`. If you have
added child pipelines, then another directory named `output/includes/` will
be created and contain all the child pipelines yaml files. When you are ready,
you would copy the content of the file(s) to your repository on gitlab
and `voilà`!

## Command line options

To customize your configuration, you can pass some additional options to the command line.
Note that any unspecified arguments will use the default, so you can specify as little
or as many arguments as you need.

-p, --preset [String] : Select a predefined size between `sm`, `md`, `lg` and `xl`.

-i, --include [Number] : Specify number of files to include. Default: 0

NOTE: Using --preset will take precedence over any of the following arguments.

-j, --jobs [Number] : Specify number of jobs. Default: 20

-s, --stages [Number] : Specify number of stages. Default: 5

-n, --needs [Number] : Specify number of needs. Default: 6

## Examples

I just want a small configuration

```js
node index.js -p sm
```

I just want a very big configuration

```js
node index.js -p xl
```

I want a configuration with 50 jobs, 2 stages and no `needs`.

```js
node index.js -j 50 -s 2 -n 0
```

I want a configuration with 1000 jobs, 40 stages and 100 `needs` + 5 child pipelines.

```js
node index.js -j 1000 -s 40 -n 100 -i 5
```

## Type of jobs

Note that for now, when you generate a config, each job it generates
will have a different default value. Some jobs will have `if` rules and
run only on master, or they will be allowed to failed or maybe they
are manuals. All of these will evolve over time and the idea is to get as
large a sample as possible.

There is currently no way to specify a type of jobs you want or not,
so feel free to edit the resulting configuration as needed.